package com.griddynamics.lviv.intern;

import java.lang.Math;

public class MyMath {
    public double sum(double a, double b) {
        return a + b;
    }

    public double sub(double a, double b) {
        return a - b;
    }

    public double mul(double a, double b) {
        return a * b;
    }

    public double div(double a, double b) {
        try {
            return a / b;
        } catch (Exception e) {
            System.out.println("DIV BY ZERO");
            return 0;
        }
    }

    public double squere(double s) {
        return mul(s, s);
    }

    public double sqrl(double s) {
        return Math.sqrt(s);
    }

    public double abs(double s) {
        return Math.abs(s);
    }

    public double sin(double s) {
        return Math.sin(s);
    }

    public double cos(double s) {
        return Math.cos(s);
    }

    public double tg(double s) {
        return Math.tan(s);
    }

    public double ctg(double s) {
        return Math.cosh(s);
    }
}
