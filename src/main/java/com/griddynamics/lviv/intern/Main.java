package com.griddynamics.lviv.intern;

import com.griddynamics.lviv.intern.MyInterface.DoCalculateInterface;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.function.UnaryOperator;

public class Main {
    static MyMath myMath = new MyMath();
    static Scanner keyboard = new Scanner(System.in);

    public static void main(String[] args) {
        ArrayList<DoCalculateInterface> calculate = new ArrayList<>();
        while (true) {
            System.out.println("Select some function press 1, press 2 if u want give a string expression(without functions)" +
                    "press 3 for clean comandline and smt else to end program");
            String answer = keyboard.next();
            DoCalculateInterface function = Main::functionAnswer;
            DoCalculateInterface expression = Main::expressionAnswer;
            DoCalculateInterface clear = Main::clearScreen;
            calculate.add(function);
            calculate.add(expression);
            calculate.add(clear);
            calculate.get(Integer.parseInt(answer) - 1).doCalculate();
            try {

            } catch (Exception e) {
                System.out.println("Enter incorrect expression, pls try again (no letter, no space hold, count ( = count))");
            }
        }

    }

    public static void WatchChoiceFunction() {
        System.out.println("1:sqrt\n" +
                "2:^\n" +
                "3:sin\n" +
                "4:cos\n" +
                "5:tg\n" +
                "6:ctg\n");
    }

    public static boolean enterValidation(String expression) {
        if (expression.matches("(.*)[a-zA-Z= ](.*)")) {
            System.out.println("Invalid expression, pls dont enter the letter, = or space hold");
            return false;
        }
        return true;
    }

    public static String doOperation(String expression, MyMath myMath) {
        Double result = null;
        String[] splits = expression.split("\\+");
        if (splits.length > 1) {
            result = (myMath.sum(Double.parseDouble(splits[0]), Double.parseDouble(splits[1])));
            System.out.println(myMath.sum(Double.parseDouble(splits[0]), Double.parseDouble(splits[1])));
        }
        String[] splits1 = expression.split("\\*");
        if (splits1.length > 1) {
            result = (myMath.mul(Double.parseDouble(splits1[0]), Double.parseDouble(splits1[1])));
            System.out.println(myMath.mul(Double.parseDouble(splits1[0]), Double.parseDouble(splits1[1])));
        }
        String[] splits2 = expression.split("\\:");
        if (splits2.length > 1) {
            result = (myMath.div(Double.parseDouble(splits2[0]), Double.parseDouble(splits2[1])));
            System.out.println(myMath.div(Double.parseDouble(splits2[0]), Double.parseDouble(splits2[1])));
        }
        String[] splits3 = expression.split("\\-");
        if (splits3.length > 1) {
            result = (myMath.sub(Double.parseDouble(splits3[0]), Double.parseDouble(splits3[1])));
            System.out.println(myMath.sub(Double.parseDouble(splits3[0]), Double.parseDouble(splits3[1])));
        }
        return result.toString();
    }

    public static void doFunction(String answer, String expression, MyMath myMath) {
        ArrayList<UnaryOperator<Double>> function = new ArrayList<>();
        UnaryOperator<Double> sqrt = x -> myMath.sqrl(x);
        UnaryOperator<Double> square = x -> myMath.squere(x);
        UnaryOperator<Double> sin = x -> myMath.sin(x);
        UnaryOperator<Double> cos = x -> myMath.cos(x);
        UnaryOperator<Double> tg = x -> myMath.tg(x);
        UnaryOperator<Double> ctg = x -> myMath.ctg(x);
        function.add(sqrt);
        function.add(square);
        function.add(sin);
        function.add(cos);
        function.add(tg);
        function.add(ctg);
        System.out.println(function.get(Integer.parseInt(answer) - 1).apply(Double.parseDouble(expression)));
    }

    public static void clearScreen() {
        try {
            Runtime.getRuntime().exec("clear");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String findAndCalculateNeighbor(String expression, int centerIndex, MyMath myMath) {
        StringBuilder left = new StringBuilder();
        int leftIndex = 0;
        StringBuilder right = new StringBuilder();
        int rightIndex = 0;
        char[] exp = expression.toCharArray();
        int i = 1;
        while ((centerIndex - i >= 0 &&
                exp[centerIndex - i] != '+' &&
                exp[centerIndex - i] != '-' &&
                exp[centerIndex - i] != ':' &&
                exp[centerIndex - i] != '*')) {
            left.append(exp[centerIndex - i]);
            leftIndex = centerIndex - i++;
        }
        left.reverse();
        i = 1;
        while ((centerIndex + i < expression.length() &&
                exp[centerIndex + i] != '+' &&
                exp[centerIndex + i] != '-' &&
                exp[centerIndex + i] != ':' &&
                exp[centerIndex + i] != '*')) {
            right.append(exp[centerIndex + i]);
            rightIndex = centerIndex + i++;
        }
        StringBuilder out = new StringBuilder();
        out.append(left);
        out.append(exp[centerIndex]);
        out.append(right);
        try {
            return expression.replace(expression.substring(leftIndex, rightIndex), doOperation(out.toString(), myMath));
        } catch (NumberFormatException e) {
            return expression;
        }
    }

    public static void functionAnswer() {
        String choice;
        String expression;
        do {
            WatchChoiceFunction();
            choice = keyboard.next();
        } while (!choice.matches("[1-6]{1}"));
        System.out.println("Enter the number");
        do {
            expression = keyboard.next();
        } while (!enterValidation(expression));
        doFunction(choice, expression, myMath);
    }

    public static Double expressionAnswer() {
        String expression;
        String myExpression;
        StringBuilder builder = new StringBuilder();
        System.out.println("Please enter the your expression");
        do {
            expression = keyboard.next();
        } while (!enterValidation(expression));

        builder.append("(");
        builder.append(expression);
        builder.append(")");
        expression = builder.toString();

        while (expression.indexOf('(') >= 0) {
            int firs = expression.lastIndexOf('(');
            int last = expression.indexOf(")", firs);
            myExpression = expression.substring(firs, last + 1);
            String mystr = myExpression;
            myExpression = myExpression.replace("(", "");
            myExpression = myExpression.replace(")", "");
            myExpression = calculate(myExpression, myMath);
            expression = expression.replace(mystr, myExpression);
        }
        return Double.parseDouble(expression);
    }

    public static String calculate(String myExpression, MyMath myMath) {
        int centerIndex;
        while ((centerIndex = myExpression.indexOf("*")) != -1)
            myExpression = findAndCalculateNeighbor(myExpression, centerIndex, myMath);
        while ((centerIndex = myExpression.indexOf(":")) != -1)
            myExpression = findAndCalculateNeighbor(myExpression, centerIndex, myMath);
        while ((centerIndex = myExpression.indexOf("-")) > 0)
            myExpression = findAndCalculateNeighbor(myExpression, centerIndex, myMath);
        while ((centerIndex = myExpression.indexOf("+")) != -1)
            myExpression = findAndCalculateNeighbor(myExpression, centerIndex, myMath);
        return myExpression;
    }
}
