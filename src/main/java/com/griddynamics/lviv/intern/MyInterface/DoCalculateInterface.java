package com.griddynamics.lviv.intern.MyInterface;
@FunctionalInterface
public interface DoCalculateInterface {
    void doCalculate();
}
